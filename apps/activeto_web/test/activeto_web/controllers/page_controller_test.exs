defmodule ActiveTOWeb.PageControllerTest do
  use ActiveTOWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Welcome to ActiveTO!"
  end
end
