defmodule ActiveTO.Repo do
  use Ecto.Repo,
    otp_app: :activeto,
    adapter: Ecto.Adapters.Postgres
end
